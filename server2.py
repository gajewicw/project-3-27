import socket
import threading
import rsa

public_key,private_key= rsa.newkeys(1024)
public_partner= None

Header_Capacity = 64
Port_Number = 300001
Server_IP = socket.gethostbyname(socket.gethostname())
Server_Address = (Server_IP, Port_Number)
Disconnect = "end"

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind(Server_Address)

def client_connection(connection, host_address):
    print("[new connection] %s connected." % str(host_address))
     
    Commands=["Info","Withdraw","Deposit","End"]
    units=50
    Capacity=100
    Space=Capacity-units
    
    connected = True
    while connected:
        message_lenght = connection.recv(Header_Capacity)
        if message_lenght:
            message_lenght = int(message_lenght)
            message_content = connection.recv(message_lenght)
            message_content = message_content #.decode('utf-8')
           
            print("[%s] %s" % (str(host_address), message_content))

            if message_content=="Info":
                connection.send(rsa.encrypt(("\n%s storage size\n%s boxes of gloves available\n%s%% storage space available\n" % (str(Capacity), str(units), str(Space)).encode(),public_partner))#.encode('utf-8')
            if message_content=="Withdraw":
                units=units-1
                Space=Capacity-units
                connection.send("\n%s storage size\n%s boxes of gloves available\n%s%% storage space available\n" % (str(Capacity), str(units), str(Space)))#.encode('utf-8') 
            if message_content=="Deposit":
                units=units+1
                Space=Capacity-units
                connection.send("\n%s storage size\n%s boxes of gloves available\n%s%% storage space available\n" % (str(Capacity), str(units), str(Space)))#.encode('utf-8')     
                
            if message_content not in Commands:
                connection.send("\n [ERROR COMMAND NOT RECOGNISED]")#.encode('utf-8')
                
                          
            if message_content == Disconnect:
                connected = False
                connection.send("closing connection with %s" % str(host_address))#.encode('utf-8')
                connection.close()

def main():
    server.listen(5)
    print(" Gloves Shelf is listening on %s" % str(Server_IP))
    while True:
        connection, host_address = server.accept()
        client.send(public_key.save_pkcs1("PEM"))
        public_partner = rsa.PublicKey.load_pkcs1(client.recv(1024))
        thread = threading.Thread(target=client_connection, args=(connection, host_address))
        thread.start()
        count = threading.activeCount() - 1
        print("[Active Nodes] %d" % count)

print(" Glove shelf server initiated...")
main()
